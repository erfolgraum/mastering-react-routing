import React from "react";
import JoinProgram from "../components/JoinProgram";
import Home from "./Home";

const JoinProgramPage = () => {
  return (
    <div>
      <Home />
      <JoinProgram />
    </div>
  );
};

export default JoinProgramPage;
