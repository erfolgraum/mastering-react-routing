import React from "react";
import Home from "./Home";
import Community from "../components/Community";


const CommunityPage = () => {
  return (
    <div>
      <Home />
      <Community />
    </div>
  );
};

export default CommunityPage;
