import { Link } from "react-router-dom";
import React from 'react'


const NotFoundPage = () => {
  return (
    <div className="not-found__wrap">
        <div className="not-found__box">
            <h2>Page Not found</h2>
            <p>Looks like you've followed a broken link or entered a URL that doesn't <br/>
                exist on this site.
            </p>
            <Link to={"/"} className="back-link">⇽ Back to our site</Link> 
        </div>
    </div>
  )
}

export default NotFoundPage