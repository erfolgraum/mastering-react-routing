import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

const Community = () => {
  const [users, setUsers] = useState([]);
  const [isSectionVisible, setIsSectionVisible] = useState(false);



  useEffect(() => {
    fetch(`/community`)
      .then((res) => {
        return res.json();
      })
      .then((arr) => {
        setUsers(arr);
      });
  }, []);

  const toggleSectionVisibility = () => {
    setIsSectionVisible((prevState) => !prevState);
  };

  return (
    <>
      <button className="btn-toggle" onClick={toggleSectionVisibility}>
        {!isSectionVisible ? "Hide section" : "Show section"}
      </button>
      {!isSectionVisible && (
        <section className="app-section app-section__users">
          <h2 className="app-title">
            Big Community of
            <br />
            People Like You
          </h2>
          <h3 className="app-subtitle app-subtitle__users">
            We’re proud of our products, and we’re really excited when we get
            feedback from our users.
          </h3>
          <div className="user__box">
            {users.map((user) => {
              console.log(user);
              const { id, avatar, firstName, lastName, position } = user;

              return (
                <div className="user" key={id}>
                  <Link to={`/community/${id}`}>
                    <img src={avatar} className="user__img" alt={id} />
                  </Link>
                  <p className="user__desc">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmod tempor incididunt ut labore et dolor.
                  </p>
                  <h4 className="user__name">
                    {firstName} {lastName}
                  </h4>
                  <div className="user__position">{position}</div>
                </div>
              );
            })}
          </div>
        </section>
      )}
    </>
  );
};

export default Community;
